import numpy as np
import time

if __name__ == '__main__':

    for i in range(1, 2, 3):
        print(i)

    print('\n')
    print('CYKLY')
    print('\n')

    for i in np.array([1, 43, -12]):
        if i > 10:
            print('Larger than 10')
        elif i < 0:
            print('Negative value')
        else:
            print('Something else')

    print('\n')
    print('ODCITAVANIE VEKTOROV OD MATIKE')
    print('\n')

    m = 50
    n = 10
    A = np.ones((m, n))
    v = 2 * np.random.rand(1, n)

    for i in range(1, m):
        A[i][:] = A[i][:] - v
    print(A)

    A = np.ones((m, n)) - np.tile(v, (m, 1))
    print(A)

    print('\n')
    print('KOPIROVANIE PRVKOV')
    print('\n')

    B = np.zeros((m, n))
    for i in range(1, m):
        for j in range(1, n):
            if A[i, j] > 0:
                B[i, j] = A[i, j]
            print(B[i][j])

    print('\n')

    B = np.zeros((m, n))
    ind = np.where(A > 0)
    B[ind] = A[ind]
    print(B[ind])

    print('\n')
    print('MERANIE CASU')
    print('\n')

    tic = time.time()
    x = np.array([0])
    for k in range(1, 100000):
        x = np.append(x, x[k - 1] + 5)
    toc = time.time()
    tic_toc = toc - tic
    print(tic_toc)

    print('\n')
    print('FUNKCIA')
    print('\n')


    def myfunction(x):
        a = np.array([-2, -1, 0, 1])
        b = a + x
        print(b)
        return b


    print('\n')


    def myotherfunction(a, b):
        y = a + b
        z = a - b
        print(y, " ", z)
        return y, z


    print('\n')

    a = np.array([1, 2, 3, 4])
    b = myfunction(2 * a)
    print(b)

    print('\n')

    a = np.array([1, 2, 3, 4])
    b = myfunction(2 * a)
    c, d = myotherfunction(a, b)
    print(c, d)

    print('\n')
    print('A ZAZVONIL ZVONEC A ROZPRAVKY BOL KONIEC')
